/* 3. Codifica la jerarquía de clases Java representada por el diagrama UML adjunto teniendo en cuenta
que:
• La clase base es la clase Empleado. Esta clase contiene:
• Un atributo protegido nombre de tipo String que heredan el resto de clases.
• Un constructor por defecto.
• Un constructor con parámetros que inicializa el nombre con el String que recibe.
• Método set y get para el atributo nombre.
• Un método toString() que devuelve el String: "Empleado " + nombre.
El resto de clases solo deben sobrescribir el método toString() en cada una de ellas y declarar el
constructor adecuado de forma que cuando se ejecuten las siguientes instrucciones:
Empleado E1 = new Empleado("Rafa"); Directivo D1 = new Directivo("Mario"); Operario OP1 = new
Operario("Alfonso"); Oficial OF1 = new Oficial("Luis"); Tecnico T1 = new Tecnico("Pablo");
System.out.println(E1); System.out.println(D1); System.out.println(OP1); System.out.println(OF1);
System.out.println(T1);
den como resultado:
Empleado Rafa Empleado Mario -> Directivo Empleado Alfonso -> Operario Empleado Luis ->
Operario -> Oficial Empleado Pablo -> Operario -> Tecnico */


class Empleado {
    //atributos
    protected String nombre;
    //constructores
    public Empleado(){ nombre = "Juan Elias";}
    public Empleado( String nombre){ this.nombre = nombre;}
    //setters y getters
    public void setNombre(String nombre){ this.nombre = nombre;}
    public String getNombre(){ return nombre;}

    public String toString(){
        return ("Empleado " + nombre + " ");
    }
    
}

class Operario extends Empleado{
    public Operario(){}
    public Operario( String nombre){super(nombre);}
    public String toString(){ return super.toString() + " -> Operario "; }
}

class Directivo extends Empleado{
    public Directivo(){}
    public Directivo( String nombre){ super(nombre);}
    public String toString(){ return super.toString() + " -> Directivo "; }
}

class Oficial extends Operario{
    public Oficial(){}
    public Oficial( String nombre){ super(nombre);}
    public String toString(){ return super.toString() + " -> Oficial "; }
}

class Tecnico extends Operario{
    public Tecnico(){}
    public Tecnico( String nombre){ super(nombre);}
    public String toString(){ return super.toString() + " -> Tecnico "; }
}

public class ej3{

    public static void main (String[] args){

        Empleado E1 = new Empleado("Rafa");
        Directivo D1 = new Directivo("Mario");
        Operario OP1 = new Operario("Alfonso");
        Oficial OF1 = new Oficial("Luis");
        Tecnico T1 = new Tecnico("Pablo");
        System.out.print(E1);
        System.out.print(D1);
        System.out.print(OP1);
        System.out.print(OF1);
        System.out.print(T1);
    }
}
