/*7. Realiza un ejercicio en el que se defina una interfaz Transporte con un método consumoViaje al cual se le pase los kilómetros realizados y devuelva el importe de su coste en euros. Define una clase Vehículo (potencia, consumo) que implemente la interfaz anterior, y dos clases Coche (puertas) y Moto (tipo) que hereden de la anterior. En los constructores de las clases derivadas debes hacer uso de super para llamar al constructor de la clase base pasándole los parámetros que éste requiera.
Realiza un programa que pruebe las clases anteriores.*/
interface Transporte
{
	public double consumoViaje(double kms);
}

class Vehiculo implements Transporte
{
	protected double potencia;
	protected double consumo;	// litros/100Km
	public Vehiculo() { potencia = 10;	consumo = 10; }
	public Vehiculo(double p, double c) { potencia=p;	consumo=c; }
	public double consumoViaje(double kms)
	{
		//System.out.println("El coste por cada " + km + " km del vehículo es de " + consumo*km + " euros.");
		return consumo*kms/100;
	}
	public String toString() 
	{
		return "Vehículo: \n" + " Potencia: " + potencia + "CV, consumo: " + consumo + "litros cada 100 kms";
	}
}

class Coche extends Vehiculo
{
	private int puertas;
	public Coche() { /* super(); */ puertas=4; }
	public Coche(double p, double c, int pu)
	{
		super(p,c);
		puertas=pu;
	}
/*
	NO NECESITAMOS DEFINIR ESTE MÉTODO PORQUE YA LO TIENE IGUAL HEREDADO	
	public double consumoViaje(double km)
	{
		System.out.println("El coste por cada " + km + " km del coche es de " + consumo*km + " euros.");
	} */
	public String toString() 
	{
		return "Vehículo: \n" + " Potencia: " + potencia + "CV, consumo: " + consumo + "litros cada 100 kms" + " Puertas: " + puertas;
	}
}

class Moto extends Vehiculo
{
	private String tipo;
	public Moto() { tipo = "Motocross"; }
	public Moto(double p, double c, String t) 
	{ 
		super(p,c);
		tipo = t; 
	}
/*	public void consumoViaje(double km)
	{
		System.out.println("El coste por cada " + km + " km de la moto es de " + consumo*km + " euros.");
	}*/
	public String toString() 
	{
		return "Vehículo: \n" + " Potencia: " + potencia + "CV, consumo: " + consumo + "litros cada 100 kms" + " Tipo: " + tipo;
	}
}

public class ej7
{
	public static void main(String[] args)
	{
		Vehiculo v = new Vehiculo();
		Coche co = new Coche(12,20,4);
		Moto mo = new Moto(8,16,"Derbi");
		
		System.out.println(v);
		System.out.println(co);
		System.out.println(mo);
		System.out.println();	
		System.out.println("El consumo del vehículo a los 100 kms es " + v.consumoViaje(100));
		System.out.println("El consumo del coche a los 100 kms es " +
		co.consumoViaje(100));
		System.out.println("El consumo de la moto a los 100 kms es " +
		mo.consumoViaje(100));
	}
}
