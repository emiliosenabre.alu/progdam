class cadena
{
	private String s;	// atributo de tipo String

	public cadena(String cad)
	{
		s=cad;
		// s=new String(cad);
	}
	public void cambia(String cad)
	{
		s=cad;
		s="a";
		cad="b";
	}
	public String gets() { return s; }
}

public class strings
{
	public static void main(String[] args) {
		String s1="hola";
		String s2="adiós";

		cadena c=new cadena(s1);
		// APARENTEMENTE ESTO ES UN PASO POR REFERENCIA ...
		c.cambia(s2);
		System.out.println(c.gets());
		// ... CON LO QUE AHORA S2 DEBIERA HABER CAMBIADO AL CONTENIDO "b" PERO NO ES ESO LO QUE MUESTRA A CONTINUACIÓN
		System.out.println(s2);
		// CONCLUSIÓN: LOS STRINGS SON UNA EXCEPCIÓN A LA NORMA QUE NOS DICE QUE TODOS LOS OBJETOS SE PASAN COMO PARÁMETROS POR REFERENCIA	
		System.exit(0);
	}
}


