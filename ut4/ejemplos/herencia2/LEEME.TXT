Probamos aquí que al intentar acceder desde la clase circulo directamente al atributo color heredado de la clase figura, no lo permite por haberse definido como privado en la clase base.

Solución: cambiar el acceso al atributo Color en la clase base al tipo protected.
