// Dado que pretendo clonar objetos dibujo, la clase debe implementar la interfaz Cloneable
public class dibujo implements Cloneable
{
	private int num;
	private rectangulo r;
	private circulo c;
	
	public dibujo() { num=1; r = new rectangulo(); c = new circulo(); }
	public dibujo(dibujo d)	// Constructor de COPIA
	{
		num = d.num;
		//r = d.r;
		//c = d.c;
		r = new rectangulo(d.getRectangulo().getAncho(),d.getRectangulo().getAlto());
		c = new circulo(d.getCirculo().getRadio());
	}
	public void setNum(int n) { num = n; }
	public String toString() { return "num: " + num + r.toString() + c.toString(); }
	public rectangulo getRectangulo() { return r; }
	public void setRectangulo(rectangulo rect) { r = rect; }
	public circulo getCirculo() { return c; }
	public void setCirculo(circulo circ) { c = circ; }
	//public void setAncho(double an) { r.setAncho(an); }
	// sobreescribo el método clone() heredado de Object
	public dibujo clone()
	{
		dibujo o = null;
		try
		{
			o = (dibujo)super.clone();
			o.setRectangulo(new rectangulo(r.getAncho(),r.getAlto()));
			o.setCirculo(new circulo(c.getRadio()));
		}
		catch(CloneNotSupportedException e)
		{
			System.err.println(e.getMessage());
		}
		return o;
	}
}
