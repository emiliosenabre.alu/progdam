abstract class forma
{
	void identidad() { System.out.println(this); } //this=el objeto; mostrará el objeto.
	//abstract public String toString();
}

class circulo extends forma
{
	public String toString() { return "circulo"; }
	public static void jerarquia(Object obj)
	{
		Object o = obj;
		while (o.getClass().getSuperclass() != null)
		{
			try
			{
				System.out.println(o.getClass() + " es una subclase de " + o.getClass().getSuperclass());
				o = o.getClass().getSuperclass().getDeclaredConstructor().newInstance();
			}
			catch (Exception e)
			{
				System.err.println("Error"); break;
			}
/*			catch (InstantiationException e)
			{
				System.out.println("Imposible instanciar la clase " + o.getClass().getSuperclass()); break;
			}
			catch (IllegalAccessException e)
			{
				System.out.println("No hay acceso"); break;
			}*/
		}
	}
	
	public static void main(String args[])
	{
		circulo c = new circulo();
		// Haciendo el upcast ... A pesar de ello ...
		forma f = /*(circulo)*/ c;	// ... sigue mostrando ser un círculo
		f.identidad();
		// haciendo el downcast ...
		if (f instanceof circulo)
			(/*(circulo)*/f).identidad();	// ... con moldeado o sin él sigue siendo circulo
		else
			System.out.println("f (forma) no es un circulo");
		jerarquia(c);
	}  
}
