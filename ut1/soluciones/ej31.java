//31.Realizar un programa que lea la fecha de nacimiento de una persona y la fecha del día actual y escriba la edad en años de la persona

public class ej31{
	public static void main (String args[])
	{
		int diaN, diaA, mesN, mesA, yearN, yearA, totalN, totalA, edad=0;
		
		//Recoger fecha de nacimiento
		System.out.println("Introduce tu fecha de nacimiento (dia, mes, a\u00f1o): ");
		diaN=Integer.parseInt(System.console().readLine());
		mesN=Integer.parseInt(System.console().readLine());
		yearN=Integer.parseInt(System.console().readLine());
		
		//Recoger fecha actual
		System.out.println("Introduce la fecha actual (dia, mes, a\u00f1o): ");
		diaA=Integer.parseInt(System.console().readLine());
		mesA=Integer.parseInt(System.console().readLine());
		yearA=Integer.parseInt(System.console().readLine());
		
		//Convertir fecha de nacimiento y fecha actual en días
		totalN=diaN+(30*(mesN-1))+(365*yearN);
		totalA=diaA+(30*(mesA-1))+(365*yearA);
		
		//Comprobar si la fecha actual es correcta
		//*SOLUCIONADO añadiendo un else if con la condición de que totalA sea mayor o igual que totalN: Falta arreglar el caso en el que la fecha de nacimiento coincida con la fecha actual
		if (totalA<totalN)
		{
			System.out.println("La fecha introducida no es valida");
			System.out.println("Introduce la fecha actual (dia, mes, a\u00f1o): ");
			diaA=Integer.parseInt(System.console().readLine());
			mesA=Integer.parseInt(System.console().readLine());
			yearA=Integer.parseInt(System.console().readLine());
		}
		
		//Realiza la diferencia para obtener la edad y la pasa a años
		//Con esta condición se soluciona la excepción que se daba cuando coincidian totalA y totalN
		else if (totalA>=totalN)
		{
		edad=totalA-totalN;
		edad=edad/365;
		}
		
		System.out.println("Tu edad es: "+edad+ " a\u00f1os");
	}
}
