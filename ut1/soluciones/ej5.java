// 5. Programa que, a partir de un entero introducido desde teclado, muestra todos los enteros, desde 1, menores que él.

public class ej5
{
	public static void main(String[] args) {
		int i,num;
		
		System.out.println("Introduce un entero:");
		num = Integer.parseInt(System.console().readLine());
		for ( i=1 ; i < num ; i++)
			System.out.println(i);
			
			
		System.exit(0);
	}
}
