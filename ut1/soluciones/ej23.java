//23. Algoritmo que permite calcular el factorial de un numero natural.
import java.util.Scanner;

public class ej23
{
	public static void main(String[] args) {
		int i,num,fact=1;
		
		Scanner tec = new Scanner(System.in);
		System.out.println("Introduce un entero positivo mayor que cero:");
		num = tec.nextInt();
		for ( i=2 ; i <= num ; i++)
			fact = fact*i;
		System.out.println("Su factorial es " + fact);
		
		
		System.exit(0);
	}
}
