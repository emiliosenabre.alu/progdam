// 24. Algoritmo que lea números desde teclado y finalice cuando se introduzca uno mayor que la suma de los dos anteriores. Escribir en pantalla el numero de valores introducidos y los valores que cumplieron la condición de finalización.

public class ej24b{

    public static void main(String args[])
    {
        double num=0,n1=0,n2=0; int cont=1;

        while (true)
        {
            System.out.println("Introduzca numero: ");
            num = Double.parseDouble(System.console().readLine());

            if (cont>=3)
                if (num > (n1+n2))
                    break;
 
            cont++;
            if ((cont%2)==0)
                n1=num;
            else
                n2=num;
        }

        System.out.println("\nSe han introducido un total de " + cont + " valores.\n"+ "El ultimo valor introducido " + num + " es mayor que la suma de los dos ultimos anteriores " + n1 + " y " + n2 + " (que suman " + (n1+n2) + ")\n");

        System.exit(0);
    }
}
