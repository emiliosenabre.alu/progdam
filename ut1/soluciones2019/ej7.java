//7. Programa que imprime todos los enteros entre 1 y 100.

public class ej7
{
	public static void main(String[] args) 
	{
		int cont;   // variable local a toda la función main
		for ( cont = 1; cont <=100; cont++)
		    System.out.println(cont);	
	}
}
