//solicita notas introducidas por teclado acabadas con un numero negativo, e imprime en pantalla el aprobado de nota más baja y el suspenso de nota más alta.
public class p13{
	public static void main(String[] args) {
		double n,maxSus=-1,minAp=11;

		System.out.println("Dame notas del 1 al 10  y te dire el aprobado con la nota mas baja y el suspenso con la mas alta (negativo para acabar):");
		do{
			
			n = Double.parseDouble(System.console().readLine());
			if(n<0)break;
			if(n>=5)	// aprobado
			{
				if( n < minAp)
					minAp = n;
			}		
			else	// suspenso
				if(n>=maxSus)
					maxSus=n;

		}while(true);
		if (minAp <= 10)
			System.out.println("La menor nota de los aprobados es "+minAp);
		if (maxSus > -1)
			System.out.println("Y la mayor nota de los suspensos es "+maxSus);
	}
}
