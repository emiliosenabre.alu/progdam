// Programa que acepte números positivos o iguales a 0 hasta acabar con un negativo y 
// muestre por pantalla el total de valores validos introducidos.

import java.util.Scanner;

public class ej19
{
	public static void main (String arg[])
	{
		int contador =0;
		double num1;
		Scanner ent = new Scanner(System.in);
		
		System.out.print("\n Introduce un número: ");
		num1 = ent.nextDouble();
		
		while (num1 >=0)
		{ 
			contador ++;
			System.out.println("PARA TERMINAR INTRODUCE UN NÚMERO NEGATIVO");
			System.out.print("\n Introduce un número: ");
			num1 = ent.nextDouble();
		}
		System.out.println("\n TOTAL DE NÚMEROS VALIDOS INTRODUCIDOS: " + contador + '\n');
	}
}
