// 26. Algoritmo que indique si un numero natural (positivo) introducido por teclado es primo.

import java.util.Scanner;

public class ej26
{
    public static void main(String[] args)
    {
    	int num; boolean esPrimo=true;
    	Scanner ent = new Scanner(System.in);
    	
    	System.out.println("Introduce un entero positivo:");
    	num = ent.nextInt();
    	for (int i=2 ; i <= num/2 ; i++)
    		if (( num % i) == 0) // división exacta ==> NO es primo
    		{
    			//System.out.println("No es primo");
    			esPrimo = false;
    			break;
    		}
    	if (esPrimo)	// equivale a esPrimo == true
    		System.out.println("Sí es primo");
    	else
    		System.out.println("No es primo");
     }
} 
