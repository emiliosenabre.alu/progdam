//24. Algoritmo que lea números desde teclado y finalice cuando se introduzca uno mayor que la suma de los dos anteriores. Escribir en pantalla el numero de valores introducidos y los valores que cumplieron la condición de finalización.

import java.util.Scanner;

public class ej24b
{

    public static void main(String Args[])
    {
    
    int cont=3;
    double num1,num2,num3;
    

    Scanner ent = new Scanner(System.in);

    System.out.println("Introduce el primer número:");
    num1=ent.nextDouble();

    System.out.println("Introduce el segundo número:");
    num2=ent.nextDouble();

    System.out.println("Introduce el tercer número:");
    num3=ent.nextDouble();


    while (num3 <= (num1+num2))
    {

        num1=num2;
        num2=num3;

        System.out.println("Introduce otro número hasta que sea mayor que la suma de los anteriores");
        num3=ent.nextDouble();
        
        cont++;
    
    }    
    

    System.out.println("Nº de numeros introducidos: "+cont);
    System.out.println(num3 + " es mayor que " +num1 + " + " +num2);

   


    }


}
