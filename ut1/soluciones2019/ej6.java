//6. Programa que acepta 2 números y, si su suma es inferior a 10, pide un tercer valor. Al final muestra la suma de los valores introducidos.

import java.util.Scanner;

public class ej6
{
	public static void main(String[] args) 
	{
		double num1, num2, num3, suma;
		Scanner ent = new Scanner(System.in);

		System.out.println("Introduce 2 números y te mostraré su suma, si la suma es menor que 10 te pediré otro y mostraré la suma final");
		num1 = ent.nextDouble();
		num2 = ent.nextDouble();

		if ((num1 + num2) < 10)
		{
			System.out.println("Introduce otro número");
			num3 = ent.nextDouble();
			suma = num1 + num2 + num3;
		}
		else    // los dos primeros suman más de 9
			suma = num1 + num2; 
        System.out.println(suma);
	}
}
