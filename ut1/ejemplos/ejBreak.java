// Ejemplo de BREAK
// Programa que obliga a introducir una nota válida (entre 0 y 10)

public class ejBreak
{
	public static void main(String[] args)
	{
		double nota;
		
		do
		{
			System.out.println("Introduce una nota (entre 0 y 10):");
			nota = Double.parseDouble(System.console().readLine());
			// si la nota es válida salgo del bucle
			if ((nota >= 0) && (nota <= 10))
				break;
		} while (true);
		
		System.exit(0);
	}
}
