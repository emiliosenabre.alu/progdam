// Programa que pida un número e indique si es positivo, negativo o cero

public class positivoNegativoCero
{
	public static void main(String[] args) 
	{
		double n;
		
		System.out.println("Introduce un número:");
		n = Double.parseDouble(System.console().readLine());
		if ( n > 0 )
			System.out.println("Es positivo");
		else
			if ( n < 0 )
				System.out.println("Es negativo");
			else
				System.out.println("Es cero");		
	}
}
