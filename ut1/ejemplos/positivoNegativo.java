// Programa que pida un número e indique si es positivo o negativo

public class positivoNegativo
{
	public static void main(String[] args) 
	{
		double n;
		
		System.out.println("Introduce un número:");
		n = Double.parseDouble(System.console().readLine());
		if ( n >= 0 )
			System.out.println("Es positivo");
		else
			System.out.println("Es negativo");
	}
}
