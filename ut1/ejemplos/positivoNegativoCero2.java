// Programa que pida un número e indique si es positivo, negativo o cero

// ¡ojo!: SOLUCIÓN NO RECOMENDADA, MENOS EFICIENTE

public class positivoNegativoCero2
{
	public static void main(String[] args) 
	{
		double n;
		
		System.out.println("Introduce un número:");
		n = Double.parseDouble(System.console().readLine());
		if ( n > 0 )
			System.out.println("Es positivo");
		if ( n < 0 )
				System.out.println("Es negativo");
		if ( 0 = n )
				System.out.println("Es cero");		
	}
}
