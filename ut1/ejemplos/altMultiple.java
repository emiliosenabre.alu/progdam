// Ejemplo de alternativa múltiple. El programa pedirá un entero (entre 1 y 7) y mostrará el nombre del día

public class altMultiple
{
	public static void main(String[] args)
	{
		int dia;
		
		System.out.println("Escribe el día de la semana (entre 1 y 7):");
		dia = Integer.parseInt(System.console().readLine());
		switch (dia)
		{
			case 1: System.out.println("Lunes");break;
			case 2: System.out.println("Martes");break;
			case 3: System.out.println("Miércoles");break;
			case 4: System.out.println("Jueves");break;
			case 5: System.out.println("Viernes");break;
			case 6: System.out.println("Sábado");break;
			case 7: System.out.println("Domingo");break;
			default: System.out.println("Valor incorrecto");
		}
			
		System.exit(0);
	}
}
