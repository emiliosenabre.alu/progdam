/* 1. Programa que muestre unas determinadas líneas de un
 fichero de texto. Tanto el fichero como los números de línea
  (en orden creciente) serán pasados como parámetros en la
   ejecución (al menos una línea). Ejemplo:

	java ej1 fichero.txt 3 7 10 */

import java.io.*;

public class ej1
{
	public static void main(String[] args) {
		if (args.length > 1)
		{
			String s; int cont = 1, i=1;

			try (
				FileReader fr = new FileReader(args[0]);
				BufferedReader br = new BufferedReader(fr);
				)
			{
				s=br.readLine();
				while (s != null)	// for(int i=1; s! null; cont++)
				{
					if ( Integer.parseInt(args[i]) == cont )
					{
						System.out.println(s);
						i++;
						if (i == args.length)
							System.exit(0);
					}
					s=br.readLine();
					cont++;
				}
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else
			System.out.println("Forma de uso: java ej1 /ruta/al/fichero lineaA lineaB lineaC ...");
	}
}