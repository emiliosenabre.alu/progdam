/* Programa que cree un fichero binario con datos de libros
(identificador, título y autor) introducidos por el
 usuario desde teclado hasta acabar con un identificador nulo */

import java.io.*;

public class ejRegistros
{
	public static void main(String[] args) {
		int cod; String tit,aut;
		InputStreamReader isr = new InputStreamReader(System.in);
 		BufferedReader ent = new BufferedReader(isr);
 		try (
 			FileOutputStream fos = new FileOutputStream("libros.dat",true);
 			 DataOutputStream dos = new DataOutputStream(fos);
 			 )
 		{
			
			System.out.println("PROGRAMA DE GESTIÓN DE LIBROS\n===================");
			System.out.println("Introduce código entero del libro (0 para acabar)");
			cod = Integer.parseInt(ent.readLine());
			while (cod != 0)
			{
				dos.writeInt(cod);
				System.out.println("Introduce título:");
				tit = ent.readLine();
				dos.writeUTF(tit);
				System.out.println("Introduce autor:");
				aut = ent.readLine();
				dos.writeUTF(aut);
				System.out.println("Introduce código entero del libro (0 para acabar)");
				cod = Integer.parseInt(ent.readLine());
			}
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());;
		}
		//if (dos )
		//
		try (
			FileInputStream fis = new FileInputStream("libros.dat");
			DataInputStream dis = new DataInputStream(fis);
		)
		{
			// while (dis.available()>0)
			while (true)
			{
				cod = dis.readInt();
				tit = dis.readUTF();
				aut = dis.readUTF();
				System.out.println("Código: " + cod + ", título: " + tit + ", autor: " + aut);
			}
		}
		catch(EOFException e)
		{
			System.out.println("Fin");
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
}
