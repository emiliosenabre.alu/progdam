/* Ejemplo inicial de escritura (y lectura) BINARIA
El programa pedirá números al usuario, hasta acabar con un cero,
 y los escribirá secuencialmente a fichero */

import java.io.*;

 public class ejDOSDIS
 {
 	public static void main(String[] args) {
 		// para lectura de teclado
 		int cont=0;	double suma=0, num;
 		FileInputStream fis=null;
 		DataInputStream dis=null;
 		InputStreamReader isr = new InputStreamReader(System.in);
 		BufferedReader ent = new BufferedReader(isr); 
 		// ara escritura a fichero
 		try
 		{
	 		FileOutputStream fos = new FileOutputStream("nums.dat",true);
	 		DataOutputStream dos = new DataOutputStream(fos);

	 		System.out.println("Introduce un número (0 para acabar):");
	 		double n = Double.parseDouble(ent.readLine());
	 		while (n != 0)
	 		{
	 			dos.writeDouble(n);
	 			System.out.println("Introduce otro número (0 para acabar):");
	 			n = Double.parseDouble(ent.readLine());
	 		}
	 		if (dos != null)
	 			{ dos.close(); fos.close();}
	 	}
	 	catch(IOException e)
	 	{
	 		System.err.println(e.getMessage());
	 	}
	 	try
	 	{
		 	fis = new FileInputStream("nums.dat");
		 	dis = new DataInputStream(fis);

		 	while(true)
		 	{
		 		num = dis.readDouble();
		 		System.out.println(num);
		 		suma += num;
		 		cont++;
		 	}
		 }
		 catch(EOFException e)
		 {
		 	try
		 	{
		 		if (dis != null) { dis.close(); fis.close();}
		 	}
		 	catch(IOException e2)
		 	{
		 		System.err.println(e2.getMessage());
		 	}
		 	System.out.println("Alcanzado el iinal del fichero");
		 }
		 catch(IOException e)
		 {
		 	System.err.println(e.getMessage());
		 }
	 		if (cont>0)
	 			System.out.println("El promedio de todos los valores es " + suma/cont);
 	}
 }
