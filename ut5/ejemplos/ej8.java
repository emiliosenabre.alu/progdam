import java.io.*;

public class ej8
{
	public static void main(String[] args)
	{
		String []amigos={"Andrés Rosique","Pedro Ruiz","Isaac Sanchez","Juan Serrano"};
		
		File f = new File("amigos.txt");
		try{
			FileWriter fw = new FileWriter(f);
			for (String s : amigos){
				fw.write(s);
				fw.write("\r\n");
			}
			if (fw != null) fw.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		//File fe = new File("amigos.txt");
		if (f.exists()){
			try{
				FileReader fr = new FileReader(f);
				BufferedReader br = new BufferedReader(fr);
				String s;
				while((s = br.readLine()) != null) {
					System.out.println(s);
				} 				
				if (fr != null) fr.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}

