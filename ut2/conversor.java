// Programa que convierta de binario a decimal, y viceversa, con dos funciones que hagan las dos conversiones.
import java.util.*;
public class conversor
{
	public static void main(String[] args)
	{
		byte opcion; long num = 0; Scanner ent = new Scanner(System.in);
		System.out.println("Para pasar de binario a decimal, introduce 1.\nDe decimal a binario introduce 2.");
		opcion = ent.nextByte();
		switch (opcion)
		{
			case 1:
				try
				{
					System.out.println("Introduce el número para convertirlo (máximo 2 bytes, 16 bits): ");
					num = ent.nextLong();
				}catch(InputMismatchException entero)
				{
					System.err.println("ERROR! Debes introducir un entero.");
					System.exit(0);
				}	
				if (binToDec(num) != 2)
					System.out.println(binToDec(num));
				else
					System.out.println("ERROR! Debes introducir un número binario");
			break;

			case 2:
				try
				{
					System.out.println("Introduce el número para convertirlo: ");
					num = ent.nextLong();
				}catch(InputMismatchException entero2)
				{
					System.err.println("ERROR! Debes introducir un entero.");
					System.exit(0);
				}	
				System.out.println(decToBin(num));
			break;

			default: System.out.println("Opción incorrecta.");
			break;
		}	
	}
	public static long binToDec(long bin)
	{
		long potencia = 1, valor = 0,digito;  byte binario = 0;

		while (binario < 16)
		{
			digito = bin % 10;
			if ((digito != 1) && (digito != 0))
				return 2;
			if (digito == 1)
				valor+=potencia;
			potencia*=2;
			bin /= 10;
			binario++;
		}
		return valor;
	}
	public static String decToBin(long dec)
	{
		long digito = 0; String binario = "";
		while (dec > 0)
		{
			digito = dec % 2;
			binario = digito + binario;
			dec /= 2;
		}
		return binario;
	}
}
