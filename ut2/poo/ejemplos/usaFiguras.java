// Programa que instancia rectángulos (crea objetos de esa clase)

public class usaFiguras
{
	public static void main(String[] args)
	{
		// creamos dos objetos rectangulo
		rectangulo r1 = new rectangulo();	// utilizo el primer constructor
		rectangulo r2 = new rectangulo(3,4);	// utilizo el segundo constructor
		rectangulo r3 = new rectangulo(5);
		// asigno tamaños a ambos rectángulos
		//r1.cambiaValores(2,3);	// asigno ancho de 2 y alto de 3
		//r2.cambiaValores(3,6);	// asigno ancho de 3 y alto de 6
		// obtener el área y el perímetro de los rectángulos
		System.out.println("El área del primer rectángulo es " + r1.area() + " y su perímetro es " + r1.perimetro());
		System.out.println("El área del segundo rectángulo es " + r2.area() + " y su perímetro es " + r2.perimetro());
		System.out.println("El área del tercer rectángulo (que es un cuadrado) es " + r3.area() + " y su perímetro es " + r3.perimetro());
		
		r1.setAlto(7);
		System.out.println("El área del primer rectángulo es " + r1.area() + " y su perímetro es " + r1.perimetro());
		System.out.println("2º rectángulo: alto = " + r2.getAlto() + ", ancho = " + r2.getAncho());
		/*
			Estas líneas darían error de compilación por ser los atributos private
		System.out.println("El ancho del 2º rectángulo es " + r2.ancho);
		r2.ancho = 22;
		System.out.println("El ancho del 2º rectángulo es " + r2.ancho); */
		/* el primer círculo se crea con el constructor sin parámetros */
		circulo c1 = new circulo();
		/* este se crea con el constructor general, el que permite seleccionar el radio */
		circulo c2 = new circulo(3);
		System.out.println("Radio: " + c1.getRadio() + ", perímetro: " + c1.perimetro() + ", área: " + c1.area());
		System.out.println("Radio: " + c2.getRadio() + ", perímetro: " + c2.perimetro() + ", área: " + c2.area());
		// Creo r4 como copia (clon) de r2
		rectangulo r4 = new rectangulo(r2);
		System.out.println("4º rectángulo: alto = " + r4.getAlto() + ", ancho = " + r4.getAncho());
		System.out.println("Se han creado " + rectangulo.contR + " rectángulos");
		
		
		System.exit(0);
	}
}
