// Clase instanciable circulo CON COLOR

public class circulo
{
	// atributos
	private final static double PI = 3.141592;
	private double radio;
	private String color;
	// métodos
		// constructores
	public circulo() { radio = 1; color="negro"; }
	// me permite seleccionar sólo el radio
	public circulo(double r) { radio = r; color="negro"; }
	// me permite seleccionar radio y color
	public circulo(double r, String c) { radio = r; color=c; }
	// setters y getters
	public void setRadio(double r) { radio = r; }
	public double getRadio() { return radio; }
	public void setColor(String c) { color = c; }
	public String getColor() { return color; }
	// resto de métodos
	public double area() { return PI*radio*radio; }
	public double perimetro() { return 2*PI*radio; }
}


	
