/* 1. Crear una clase Reloj (hora,minutos,segundos) que permita al menos:

 - crear relojes con hora inicial las 12 del mediodía
 - crear relojes con hora inicial a elegir
 - cambiar sólo la hora, sólo los minutos o sólo los segundos (setters)
 - obtener sólo el valor de hora, o de minutos o de segundos (getters)
 - obtener los segundos transcurridos desde las 12 de la medianoche()
 - añadir una cantidad de tiempo expresada en segundos (vigila que los minutos o segundos no excedan de 59, ni las horas de 23).
 	Ejemplo:
 	si el reloj marca las 10:35:24 y se le añaden 1810 segundos (media hora y 10 segundos) quedará en 11:05:34
 - ... (otras operaciones que se te ocurran útiles)
 
 Crea un programa que instancie relojes poniendo a prueba toda la clase anterior. */
 
 class reloj
 {
 	// 3 atributos enteros (hora, minutos y segundos)
 	private int h;
 	private int m;
 	private int s;
 	// métodos
 		// constructores
 	public reloj() { h=12; m=s=0; }
 	public reloj(int h,int minutos,int segundos)
 	{ 
 		/* this es palabra reservada del lenguaje que actúa como una
 		referencia al objeto en el que estoy
 		this.h se refiere al atributo del objeto, mientras que
 		h es el parámetro */
 		if ((h < 0) || (m < 0) || (s < 0) || (h> 23) || (m > 59) || (s > 59))
 		{ 		
 		this.h=12; m=s=0; return; }
 		this.h=h;
 		m = minutos; s = segundos;
 	}
 		// setters
 	public void setHora(int h) { this.h = h%24; }
 	public void setMinutos(int m)
 	{ 
 		if ((m >= 0) && (m< 60))
 		 this.m = m;
 	}
 	public void setSegundos(int s)
 	{ 
 		if ((s >= 0) && (s< 60))
 			this.s = s;
 	}
 	public void setTiempo(int hora,int minutos,int segundos)
 	{ 
 		h = hora%24;
 		if ((m >= 0) && (m< 60))
 		 m = minutos;
 		if ((s >= 0) && (s< 60)) 
 		 s = segundos;
 	}
 		// getters
 	public int getHora() { return h; }
 	public int getMinutos() { return m; }
 	public int getSegundos() { return s; }
 		// resto de métodos
 	public int totalSeg() { return h*3600 + m*60 + s; }
 	public void masSeg(int seg)
 	{
 		int total = h*3600 + m*60 + s + seg;	

 		h = ((total)%86400)/3600;
        m =(((total)%86400)%3600)/60;
        s = (((total)%86400)%3600)%60;
 	}
 	/* public void masSeg(int horas, int minutos, int seg)
 	{
 		
 	} */
 	
 	public void muestraHora()
 	{
 		System.out.printf("%02d:%02d:%02d\n",h,m,s);
 	}
 }
 
 public class ej1
 {
 	public static void main(String[] args) {
 		reloj r1 = new reloj();	// 12:00:00
 		reloj r2 = new reloj(11,20,20);
 		r1.muestraHora();
 		r2.muestraHora();
 		r1.masSeg(2*3600+30*60+15);
 		r1.muestraHora();
 		System.out.println("2º reloj, son las " + r2.getHora() + " horas, " + r2.getMinutos() + " minutos y " + r2.getSegundos() + " segundos.");
 		r2.setHora(23);
 		r2.setMinutos(59);
 		r2.setSegundos(59);
 		r2.muestraHora();
 		r2.masSeg(365*86400-1);
 		r2.muestraHora();
 		System.exit(0);
 	}
 }
