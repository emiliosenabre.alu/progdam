/* 4. Realizar un programa que pida un número al usuario y dé la lista de los factoriales de los números desde el pedido hasta el 1, indicando cuáles de los factoriales hallados tienen algún dígito igual a 2.
	Se utilizará una función para el cálculo del factorial de cada número y otra para saber si el factorial calculado tiene o no algún dígito igual a 2.
	Ejemplo para el 5:

	5: factorial = 120	SI tiene 2
	4: factorial = 24	SI tiene 2
	3: factorial = 6	NO tiene 2
	2: factorial = 2	SI tiene 2
	1: factorial = 1	NO tiene 2 */

import java.util.Scanner;

public class ej4
{
	public static int factorial(int n)
	{
		if (n > 1)
			return n*factorial(n-1);
		else
			return 1;
	}
	
	public static boolean tiene2(int n)
	{
		while (n > 0)
		{
			if ((n % 10)==2)
				return true;
			n = n/10;	// pierdo el dígito de las unidades
		}
		return false;	
	}
	
	public static void main(String[] args) {
		int num, fact;
		
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduce un entero:");
		num = ent.nextInt();
		while (num > 0)
		{
			fact = factorial(num);
			num--;
			if (tiene2(fact))
				System.out.println(fact + " SI tiene 2");
			else
				System.out.println(fact + " NO tiene 2");
		}
	}
}
