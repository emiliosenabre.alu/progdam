//4.Realizar una función que reciba una cantidad de horas, 
//minutos y segundos y la pase a segundos.
import java.util.Scanner;
public class ej3
{
	public static int conversor (int h, int m, int s)
	{
		h = h *3600;
		m = m * 60;
		return h+m+s;
	}
	public static void main (String args[])
	{
		int horas,min,sec,res;
		Scanner sc = new Scanner(System.in);
		System.out.println("Dame una hora (con minutos y segundos): ");
		horas = sc.nextInt();
		min = sc.nextInt();
		sec = sc.nextInt();
		res = conversor(horas, min, sec);
		System.out.println("La hora introducida equivale a "+res+ " segundos");
	}
}
