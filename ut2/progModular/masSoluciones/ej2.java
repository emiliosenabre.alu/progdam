//2. Realizar una función que reciba un número real (double) y devuelva el número entero más próximo. Ejemplo 7,5 = 8 y 7,4=7

import java.util.*;

public class ej2
{
    public static void main(String args[])
    {
        Scanner ent=new Scanner(System.in);
        double num;
        System.out.println("Introduzca número con decimales:");

        num = ent.nextDouble();
        System.out.println("El entero más cercano es " + devEntero(num));

        System.exit(0);
    }

    public static int devEntero(double n)
    {
        if ((n - (int)n)<0.5)
            return (int)n;
        else
            return (int)n+1;
    }
}

