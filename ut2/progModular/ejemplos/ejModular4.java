/* Ejemplo MODULAR de función de tipo void

Programa que pida un entero positivo y un caracter y muestre en pantalla ese número de veces el caracter elegido */

public class ejModular4
{
	public static void main(String[] args)
	{
		// variables LOCALES a main
		int n; char c;
		
		System.out.println("Introduce un entero positivo:");
		n = Integer.parseInt(System.console().readLine());
		System.out.println("Introducir caracter:");
		// forma de leer un caracter
		c = System.console().readLine().charAt(0);
		// LLAMADA a la función
		imprimeCaracteres(c,n);
		System.out.println();
		
		System.exit(0);
	}
	
	// DEFINICIÓN de la función
	public static void imprimeCaracteres(char c, int num)
	{
		// variable LOCAL a imprimeCaracteres
		int i;
		
		for (i=1 ; i <= num; i++)
			System.out.print(c);
	}
}
