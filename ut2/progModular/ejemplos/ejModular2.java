/* Ejemplo MODULAR de función de tipo void

Programa que pida un entero positivo y muestre en pantalla ese número de veces el caracter asterisco (*) */

public class ejModular2
{
	public static void main(String[] args)
	{
		int n;
		
		System.out.println("Introduce un entero positivo");
		n = Integer.parseInt(System.console().readLine());
		// LLAMADA a la función
		imprimeAsteriscos(n);
		
		System.exit(0);
	}
	
	// DEFINICIÓN de la función
	public static void imprimeAsteriscos(int num)
	{
		int i;
		
		for (i=1 ; i <= num; i++)
			System.out.print('*');
	}
}
