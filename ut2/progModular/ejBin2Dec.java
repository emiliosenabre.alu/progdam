//ejercicio propuesto por Ricardo : hacer ej con 2 métodos, uno para pasar de binario a decimal(bin2dec) y otro para pasar de decimal a binario (dec2bin)
import java.util.Scanner;
public class ejBin2Dec
{
	public static void main(String[] args)
	{	
		String c; int num;
		Scanner sc = new Scanner(System.in);
		//UN menú inicial simple
		System.out.println("Convertir de binario a decimal (bd):");
		System.out.println("Convertir de decimal a binario (db):");
		c = sc.nextLine();
		//System.out.println(c);

		//Obtención del número que vamos a convertir
		System.out.println("¿ Qué número quieres convertir ?");
		num = sc.nextInt();

		if (c=="bd")
			System.out.println(bin2Dec(num));
		else if (c=="db")
			System.out.println(dec2Bin(num));


	}
	//método conversor de binario a decimal 
	public static int bin2Dec (int n)
	{
		if (n!=0) //aún por resolver..
			return n%10*bin2Dec(n-(n-1));
		else 
			return 1;
	}
	// método conversor de decimal a binario
	public static int dec2Bin (int n)
	{
		if (n/2 > 1)
			return dec2Bin(n/2);
		else if (n==1)
			return 1;
		else
			return 0;
	}


}
