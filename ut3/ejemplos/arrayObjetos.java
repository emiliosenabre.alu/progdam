// Programa de ejemplo de array no básico, sino ARRAY DE OBJETOS (rectangulo)

class rectangulo
{
	// atributos o propiedades (por defecto, private)
	private double ancho;
	private double alto;
	// Constructor por defecto
	public rectangulo() { ancho=alto=1;}
	// Constructor general
	public rectangulo(double an,double al)
	{
		ancho=an;
		alto=al;
	}
	// constructor para hacer un cuadrado
	public rectangulo(double lado)
	{
		ancho=alto=lado;
	}
	// constructor DE COPIA
	public rectangulo(rectangulo r)
	{
		ancho = r.ancho;
		alto = r.alto;
	}
	// métodos o funciones: definen el comportamiento (por defecto, public)
	// métodos SETTERS
	public void setAncho(double an) { ancho = an; }
	public void setAlto(double al) { alto = al; }
	// métodos GETTERS
	public double getAncho() { return ancho; }
	public double getAlto() { return alto; }
	public double area()
	{
		return ancho * alto;
	}
	public double perimetro()
	{
		return 2*(ancho + alto);
	}
	// método que me permita cambiar ancho y alto
	public void muestra()
	{
		System.out.println("Ancho: " + ancho + ", alto: " + alto);	
	}
}

public class arrayObjetos
{
	private static final int TAM = 4;
	public static void main(String[] args) 
	{
		rectangulo rs[] = new rectangulo[TAM];
		// creo rectángulos de tamaños 1x2, 2x3, 3x4 ...
		for (int i = 0 ; i < TAM; i++)
			rs[i] = new rectangulo(i+1, i+2);
		for (int i = 0 ; i < TAM; i++)
			rs[i].muestra();
		System.out.println("El área del tercer rectángulo es " + rs[2].area());
		
		
	}	
}

