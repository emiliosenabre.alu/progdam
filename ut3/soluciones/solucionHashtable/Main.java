/* 1.	Realiza un programa que, mediante la clase Hashtable, permita gestionar películas de una videoteca o videoclub. Para ello, se utilizarán entradas de tipo <Integer,String> tal que cada película tenga un número de película y su título.

	El programa permitirá:
	- introducir películas desde teclado
	- listar todas las películas
	- eliminar una película a partir de su número
	- consultar el título de una película a partir de su número */
import java.util.*;

public class Main{
	public static void main(String[] args){
		
		Peliculas pelis = new Peliculas();
		Scanner sc = new Scanner(System.in);
		String titulo = null;
		
		limpiaPantalla(300);
		
		while(true){
			System.out.println("----------------------------------------\nAdministrador de películas pendientes:\n----------------------------------------\n1-Insertar Película\n2-Eliminar Película\n3-Mostrar lista completa\n4-Salir del programa\n----------------------------------------");
			System.out.print("Introduzca la opción deseada:");
			switch(sc.nextInt()){
				case 1: 
					System.out.println("Inserte el título de la película: ");
					sc.nextLine();	// por el problema del buffer de teclado
					titulo = sc.nextLine();
					pelis.insertarPelicula(titulo);
					System.out.println("La pelicula '"+titulo+"' ha sido introducida con éxito");
					limpiaPantalla(5000);
					break;
				case 2: 
					System.out.print("Seleccione el número de la película a eliminar:");
					pelis.eliminarPelicula(sc.nextInt());
					System.out.print("La película ha sido eliminada!");
					limpiaPantalla(1500);
					break;
				case 3: 
					limpiaPantalla(300);
					System.out.println(pelis);
					break;

				case 4: 
					System.out.println("Saliendo del programa");
					limpiaPantalla(2000);
					System.exit(0); break;
				default: System.out.println("Opción incorrecta");
			}
		}
	}
	
  //Función para limpiar la terminal, creada por Ruben Muñoz
  //Se le pasa por parámetro un int con los ms que va a tardar en ejecutarse
	static void limpiaPantalla(int t){
	    try{Thread.sleep(t);} 
	    catch(InterruptedException e){e.printStackTrace();}
	    System.out.print("\033[H\033[2J"); 
	    System.out.flush();
	  }
}
