//	3. Implemente un programa que indique si una palabra es un palíndrome . Una palabra es palíndrome si se lee igual de izquierda a derecha que de derecha a izquierda. 

public class ej3
{
	public static void main(String[] args)
	{
		System.out.println("Introduce una palabra");
		String s=System.console().readLine();
		// obtengo un array de caracteres con el contenido del String
		char cad[] = s.toCharArray();
		for (int i=0 ; i < (cad.length)/2 ; i++)
			if (cad[i] != cad[cad.length-1-i])
			{
				System.out.println("No es palíndrome");
				return;
			}
		System.out.println("Sí es palíndrome");		
	}
}
